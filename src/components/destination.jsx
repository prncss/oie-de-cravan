import React, { useContext, useState } from 'react';
import { useEffect } from 'react';

function setDestinationStorage(destination) {
  const storage = window.localStorage;
  storage.setItem('destination', destination);
  storage.setItem('time', Date.now());
}

const expire = 20000;

function getDestinationStorage() {
  // return 'unset'
  const storage = window.localStorage;
  const timeStr = storage.getItem('time');
  if (!timeStr) return;
  const time = Number.parseInt(timeStr);
  const now = Date.now();
  if (now - time > expire) return;
  storage.setItem('time', now);
  return storage.getItem('destination');
}

const Context = React.createContext(null);

export function DestinationProvider({ children }) {
  const [destination, setDestinationState] = useState(undefined);
  const setDestination = (destination) => {
    setDestinationState(destination);
    setDestinationStorage(destination);
  };
  useEffect(() => {
    setDestinationState(getDestinationStorage());
  }, []);
  return (
    <Context.Provider value={[destination, setDestination]}>
      {children}
    </Context.Provider>
  );
}

export function useDestination() {
  const res = useContext(Context);
  if (!res)
    throw new Error(
      'useDestination should be case wihtin a DestinationProvider',
    );
  return res;
}

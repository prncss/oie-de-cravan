const accents = require('remove-accents');

function typo_ajust(str) {
  if (typeof str === 'string') return str.replace(/'/g, '’');
  return str;
}

function normalize(str) {
  return str
    ? accents
        .remove(str)
        .replace(/[()-.]/g, ' ')
        .replace(/(\s|^)\S{1,3}(\s|$)/g, ' ')
    : '';
}

module.exports = {
  typo_ajust,
  normalize,
};

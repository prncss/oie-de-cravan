import u from './util';

test("typo_ajust should relace ' by ’", () => {
  expect(u.typo_ajust("l'école del l'oie")).toBe("l’école del l’oie");
});
test('normalize should remove acents', () => {
  expect(u.normalize('Élève')).toBe('Eleve');
});
test('normalize should replace "().-" characters with spaces', () => {
  expect(u.normalize('(ppp.qqq-)rrr')).toBe(' ppp qqq  rrr');
});
test('normalize should remove words of less than 3 characters', () => {
  expect(u.normalize('abcd ef hij')).toBe('abcd hij');
});

# Oie de Cravan's website

L'Oie de Cravan is a poetry editor. This is the new website's source code, build with Gatsby, with Airtable as CMS and deployed on gitlab with CI/CD. (If your reading this from github, this is obiously a mirror of the intended [repo](https://gitlab.com/oiedecravan-groupe/oiedecravan-groupe.gitlab.io).)

Running verion can be seen [here](https://www.oiedecravan.com/).

Graphism is provides by [LOKI design](https://www.lokidesign.net/), which I implemented as reusable components. Layout is responsive.

## Features

- bilingual
- client side search
- self-hosted fonts
- theming system
- CMS with Airtable

## Known issues

### Fontsource

`font-display` is set to `swap` and cannot be changed, which causes font flickering on first load. According to [issue #121](https://github.com/fontsource/fontsource/issues/121) this should be solved with the next release.

### Airtable

Airtable has it's own propriatery markdown engine. `_word _rest of the text` will italicize 'word' in airtable live editor, but not with any of the open source markdown engine I have tested. This requires the client to avoid selecting spaces while they italicize a word in the live editor.

Markdown is notoriously not ergonomical for displaying verses. It is however a required feature for this website. We must use `remark-breaks` plugin, which further increases discrepency between live edition in airtable and final display on the website. It is a likely cause of typographical errors for the clients and contents must be reviewed carefully.

The `gatsby-source-airtable` have a minor issue.

It deals badly with column names that are not valid GraphQl identifiers, introducing different problems depending of the configuration options relative to that column. The safest is to set column names in airtable to their unpalatable yet computer friendly version, replacing non-word chararcters by underscores.

Despite these inconvenients, client is quite happy having the new website well integreted to their preexisting workflow.
